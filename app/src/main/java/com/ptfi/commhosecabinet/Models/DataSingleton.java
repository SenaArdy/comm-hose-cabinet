package com.ptfi.commhosecabinet.Models;

import com.ptfi.commhosecabinet.Utils.Helper;

/**
 * Created by senaardyputra on 8/1/16.
 */
public class DataSingleton {

    private String appName;
    private static DataSingleton instance;

    private int selectedYear;
    private int selectedMonth;
    private int selectedDay;
    private int selectedHour;
    private int selectedMinute;

    private boolean isAgreeJSA = false;
    private boolean isAgreeSOP = false;

    // selected panel di details data entry
    private String selectedPanel;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getSelectedPanel() {
        return selectedPanel;
    }

    public void setSelectedPanel(String selectedPanel) {
        this.selectedPanel = selectedPanel;
    }

    public static DataSingleton getInstance() {
        if (instance == null) {
            instance = new DataSingleton();
        }

        return instance;
    }

    public String getFormattedDateTime() {
        return Helper.formatNumber(selectedDay) + " "
                + Helper.numToMonthName(selectedMonth + 1) + " "
                + Helper.formatNumber(selectedYear) + " "
                + Helper.formatNumber(selectedHour) + ":"
                + Helper.formatNumber(selectedMinute);
    }

    public String getFormattedDate() {
        return Helper.formatNumber(selectedDay) + " "
                + Helper.numToMonthName(selectedMonth + 1) + " "
                + Helper.formatNumber(selectedYear);
    }

    public String getFormattedDateNextInspection() {
        return Helper.formatNumber(selectedDay + 15) + " "
                + Helper.numToMonthName(selectedMonth + 1) + " "
                + Helper.formatNumber(selectedYear);
    }

    public String getFormattedTime() {
        return Helper.formatNumber(selectedHour) + ":"
                + Helper.formatNumber(selectedMinute);
    }

    public void setDateTime(int year, int month, int day, int hour, int minutes) {
        selectedDay = day;
        selectedHour = hour;
        selectedYear = year;
        selectedMonth = month;
        selectedMinute = minutes;
    }

    public void setTime(int hour, int minutes) {
        selectedHour = hour;
        selectedMinute = minutes;
    }

    public void setDate(int year, int month, int day) {
        selectedDay = day;
        selectedYear = year;
        selectedMonth = month;
    }
}
