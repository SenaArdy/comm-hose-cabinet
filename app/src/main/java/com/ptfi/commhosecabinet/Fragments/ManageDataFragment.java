package com.ptfi.commhosecabinet.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.commhosecabinet.Database.DataSource;
import com.ptfi.commhosecabinet.MainMenu;
import com.ptfi.commhosecabinet.Models.InspectorModel;
import com.ptfi.commhosecabinet.PopUp.PopUp;
import com.ptfi.commhosecabinet.R;
import com.ptfi.commhosecabinet.SplashScreen;
import com.ptfi.commhosecabinet.Utils.CSVHelper;
import com.ptfi.commhosecabinet.Utils.FoldersFilesName;
import com.ptfi.commhosecabinet.Utils.Helper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.thinkti.android.filechooser.FileChooser;

/**
 * Created by senaardyputra on 8/1/16.
 */
public class ManageDataFragment extends Fragment {

    protected FragmentActivity mActivity;

    static TextView imeiCode, versionCode;

    static TextView inspectorDataTV;

    static TextView inspectorDataCV;

    static LinearLayout inspectorDataLL;

    static CardView inspectorInformation;

    static ImageView importInspectorIV;

    private static final int FILE_SELECT_CODE = 12332;

    private Helper.Lookup importType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        position = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_data_management, container, false);

        PopUp.pageComm = PopUp.page.generateData;

        versionCode = (TextView) rootView.findViewById(R.id.versionCode);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionCode.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiCode = (TextView) rootView.findViewById(R.id.imeiCode);
        imeiCode.setText("Device IMEI : " + deviceID);

        inspectorDataTV = (TextView) rootView.findViewById(R.id.inspectorDataTV);
        inspectorDataLL = (LinearLayout) rootView.findViewById(R.id.inspectorDataLL);
        inspectorDataCV = (TextView) rootView.findViewById(R.id.inspectorDataCV);
        inspectorInformation = (CardView) rootView.findViewById(R.id.inspectorInformation);
        importInspectorIV = (ImageView) rootView.findViewById(R.id.importInspectorIV);

        inspectorDataCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inspectorInformation.getVisibility() == View.GONE) {
                    inspectorInformation.setVisibility(View.VISIBLE);
                } else if (inspectorInformation.getVisibility() == View.VISIBLE) {
                    inspectorInformation.setVisibility(View.GONE);
                }
            }
        });

        importInspectorIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importType = Helper.Lookup.inspectorData;
                showFileChooser();
            }
        });

        initLayout();

        return rootView;
    }

    private void initLayout() {
        InitLayoutAsyncTask initLayout = new InitLayoutAsyncTask();
        initLayout.execute();
    }

    public class InitLayoutAsyncTask extends AsyncTask<Void, Integer, Void> {
        File importFolderPath;
//		String importFolderPaths;

        public InitLayoutAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Helper.showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(Void... params) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String[] listLookupFileName = new String[]{
                            FoldersFilesName.INSPECTOR_FILE_NAME};

                    TextView[] listTextViewTotalColumn = new TextView[]{
                            inspectorDataTV};

                    LinearLayout[] listTableExampleLookup = new LinearLayout[]{
                            inspectorDataLL};

                    for (int h = 0; h < listLookupFileName.length; h++) {
                        importFolderPath = new File(FoldersFilesName.EXTERNAL_ROOT_FOLDER + "/"
                                + FoldersFilesName.IMPORT_FOLDER_NAME + "/"
                                + listLookupFileName[h]);
                        if (!importFolderPath.exists())
                            Helper.copyFileAsset(listLookupFileName[h],
                                    FoldersFilesName.IMPORT_FOLDER_NAME, mActivity);

                        List<String[]> readResult = CSVHelper
                                .readCSVFromPath(importFolderPath.getAbsolutePath());
                        if (readResult != null) {
                            String[] header = null;
                            String[] example;

                            if (readResult.size() > 0) {
                                header = readResult.get(0);
                            }
                            if (readResult.size() > 1) {
                                example = readResult.get(1);
                            } else {
                                example = new String[header.length];
                            }

                            if (header != null)
                                listTextViewTotalColumn[h].setText("Total Column : "
                                        + String.valueOf(header.length));
                            int px = Math.round(TypedValue.applyDimension(
                                    TypedValue.COMPLEX_UNIT_DIP, 5, mActivity
                                            .getResources().getDisplayMetrics()));

                            listTableExampleLookup[h].removeAllViews();
                            for (int i = 0; i < header.length; i++) {
                                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                                        ViewGroup.LayoutParams.WRAP_CONTENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT);

                                LinearLayout headerLayout = new LinearLayout(
                                        mActivity);
                                headerLayout.setLayoutParams(layoutParams);
                                headerLayout.setOrientation(LinearLayout.VERTICAL);
                                LinearLayout.LayoutParams tvParamsWrap = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                LinearLayout.LayoutParams tvParamsMatch = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);

                                LinearLayout contentLayout = new LinearLayout(
                                        mActivity);
                                contentLayout.setLayoutParams(layoutParams);
                                contentLayout.setPadding(px, px, px, px);

                                TextView headerTV = new TextView(mActivity);
                                headerTV.setBackgroundResource(R.drawable.bordered_background_dark_gray);
                                headerTV.setGravity(Gravity.CENTER);
                                headerTV.setPadding(px, px, px, px);
                                headerTV.setText(header[i]);
                                headerTV.setTextColor(getResources().getColor(R.color.White));
                                headerLayout.addView(headerTV);

                                TextView exampleTV = new TextView(mActivity);
                                exampleTV
                                        .setBackgroundResource(R.drawable.transparent_bordered_background_white);
                                exampleTV.setGravity(Gravity.CENTER);
                                exampleTV.setPadding(px, px, px, px);
                                exampleTV.setText(example[i]);

                                if (header[i] != null && example[i] != null) {
                                    if (header[i].length() >= example[i].length()) {
                                        headerTV.setLayoutParams(tvParamsWrap);
                                        exampleTV.setLayoutParams(tvParamsMatch);
                                    } else {
                                        headerTV.setLayoutParams(tvParamsMatch);
                                        exampleTV.setLayoutParams(tvParamsWrap);
                                    }
                                } else {

                                }

                                headerLayout.addView(exampleTV);
                                listTableExampleLookup[h].addView(headerLayout);
                            }
                        }
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Helper.progressDialog.dismiss();

        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(mActivity, FileChooser.class);
        ArrayList<String> extensions = new ArrayList<String>();
        extensions.add(".csv");
        intent.putExtra("storagePath", FoldersFilesName.IMPORT_FOLDER_ON_EXTERNAL_PATH
                + "/" + FoldersFilesName.IMPORT_FOLDER_NAME);
        intent.putStringArrayListExtra("filterFileExtension", extensions);
        startActivityForResult(intent, FILE_SELECT_CODE);
    }

    public class ImportFileAsyncTask extends AsyncTask<Void, Integer, Void> {
        String path;
        String title = "";
        String errorMessage = "";
        Helper.CustomProgressDialog pd;
        boolean statusImport = true;

        public ImportFileAsyncTask(String path) {
            this.path = path;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = Helper.getCostumProgressDialog(mActivity);
            pd.getDialog().show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (new File(path).exists()) {
                List<String[]> readResult = CSVHelper.readCSVFromPath(path);
                if (readResult != null) {

                    int skippedData = 0;
                    DataSource dataSource = new DataSource(
                            mActivity);
                    dataSource.open();
                    switch (importType) {
                        case inspectorData :
                            title = "Inspector Data";
                            pd.setTextTitle("Import Inspector Data");
                            if (readResult.get(0).length < 3) {
                                statusImport = false;
                                Helper.showPopUpMessage(
                                        mActivity,
                                        "Error",
                                        "Number of column in csv is not equal with database table, number of column should be 4",
                                        null);
                            } else {
                                dataSource.deleteAllDataInspector();
                                for (int data = 1; data < readResult.size(); data++) {
//                                    if (skippedData < 1) {
//                                        skippedData++;
//                                        continue;
//                                    }
                                    String[] strings = readResult.get(data);
                                    InspectorModel om = new InspectorModel();
                                    om.setNameIns(strings[0]);
                                    om.setIdIns(strings[1]);
                                    om.setTitleIns(strings[2]);
                                    om.setOrgIns(strings[3]);
                                    om.id = data;
                                    long dataImport = dataSource.insertInspector(om);
                                    om.id = (dataImport);

                                    pd.setTextProgress("Import Inspector Data row " + skippedData + " from " + readResult.size() + " row");
                                    pd.setProgress((skippedData / readResult.size()) * 100);
                                }
                                mActivity.finish();
                                Intent intent = new Intent(mActivity,
                                        SplashScreen.class);
                                startActivity(intent);
                                mActivity.finish();

                            }
                            break;

                    }
                    dataSource.close();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pd.getDialog().dismiss();
            if (statusImport)
                Helper.showPopUpMessage(
                        mActivity,
                        "INFORMATION",
                        "Import "
                                + title
                                + " successed"
                                + (errorMessage.equalsIgnoreCase("") ? ""
                                : ("\nwith some row cannot import because mine area id is empty at row : \n" + errorMessage)),
                        null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILE_SELECT_CODE && resultCode == mActivity.RESULT_OK) {
            String fileSelected = data.getStringExtra("fileSelected");
            ImportFileAsyncTask exec = new ImportFileAsyncTask(fileSelected);
            exec.execute();
        }
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from Commissioning Hose Cabinet Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("BACK TO MENU", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(mActivity, MainMenu.class);
                mActivity.startActivity(intent);
                mActivity.finish();
            }
        });

        builder.show();
    }
}
