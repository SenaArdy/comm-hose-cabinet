package com.ptfi.commhosecabinet.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.commhosecabinet.Database.DataSource;
import com.ptfi.commhosecabinet.Fragments.HoseCabinetFragment;
import com.ptfi.commhosecabinet.Models.ComplianceModel;
import com.ptfi.commhosecabinet.PopUp.PopUp;
import com.ptfi.commhosecabinet.PopUp.PopUpComplianceCallback;
import com.ptfi.commhosecabinet.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 7/30/16.
 */
public class ListComplianceAdapter extends RecyclerView.Adapter<ListComplianceAdapter.dataViewHolder> {

    private static Activity mActivity;
    private static ArrayList<ComplianceModel> complianceData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView findingsTV;
        TextView remarkTV;
        TextView responsibilityTV;
        TextView statusTV;
        ImageView statusIV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.findingsTV = (TextView) itemView.findViewById(R.id.findingsTV);
            this.remarkTV = (TextView) itemView.findViewById(R.id.remarkTV);
            this.responsibilityTV = (TextView) itemView.findViewById(R.id.responsibilityTV);
            this.statusTV = (TextView) itemView.findViewById(R.id.statusTV);
            this.statusIV = (ImageView) itemView.findViewById(R.id.statusIV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String equipment = complianceData.get(position).getEquipment();
            String date = complianceData.get(position).getDate();
            final String register = complianceData.get(position).getRegister();
            final String findings = complianceData.get(position).getFindings();
            final String remark = complianceData.get(position).getRemark();
            final String responsibility = complianceData.get(position).getResponsibility();
            final String status = complianceData.get(position).getDone();

            PopUp.showCompliance(mActivity, equipment, date, register, findings, remark, responsibility, status, false, new PopUpComplianceCallback() {
                @Override
                public void onDataSave(ComplianceModel model) {
                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    ds.updateComplianceTest(model, findings, remark, responsibility, status);
                    HoseCabinetFragment.findingRv();

                    ds.close();
                }
            });
        }
    }

    public ListComplianceAdapter(Activity mActivity, ArrayList<ComplianceModel> complianceData) {
        super();
        this.mActivity = mActivity;
        this.complianceData = complianceData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_compliance_data, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        CardView listCV = holder.listCV;
        TextView findingsTV = holder.findingsTV;
        TextView remarkTV = holder.remarkTV;
        TextView responsibilityTV = holder.responsibilityTV;
        TextView statusTV = holder.statusTV;
        ImageView statusIV = holder.statusIV;

        findingsTV.setText(complianceData.get(position).getFindings());
        remarkTV.setText(complianceData.get(position).getRemark());
        responsibilityTV.setText("Responsibility : " + complianceData.get(position).getResponsibility());
        statusTV.setText("Status : " + complianceData.get(position).getDone());
        if (complianceData.get(position).getDone().equalsIgnoreCase("Done")) {
            statusIV.setBackground(mActivity.getResources().getDrawable(R.drawable.done_icons));
        } else {
            statusIV.setBackground(mActivity.getResources().getDrawable(R.drawable.not_done_icons));
        }
    }

    @Override
    public int getItemCount() {
        return complianceData.size();
    }
}
