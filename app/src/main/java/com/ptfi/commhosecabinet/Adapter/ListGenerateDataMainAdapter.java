package com.ptfi.commhosecabinet.Adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptfi.commhosecabinet.Models.HoseCabinetModel;
import com.ptfi.commhosecabinet.PopUp.GeneratePopUp;
import com.ptfi.commhosecabinet.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 7/30/16.
 */
public class ListGenerateDataMainAdapter extends RecyclerView.Adapter<ListGenerateDataMainAdapter.dataViewHolder> {

    private static Activity mActivity;
    private static ArrayList<HoseCabinetModel> sprinklerData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView equipmentTV;
        TextView locationTV;
        TextView dateTV;
        TextView registerTV;
        TextView typeTV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.equipmentTV = (TextView) itemView.findViewById(R.id.equipmentTV);
            this.locationTV = (TextView) itemView.findViewById(R.id.locationEQTV);
            this.dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            this.registerTV = (TextView) itemView.findViewById(R.id.registerTV);
            this.typeTV = (TextView) itemView.findViewById(R.id.typeTV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String equipment = sprinklerData.get(position).getEquipment();
            String date = sprinklerData.get(position).getDate();
            String location = sprinklerData.get(position).getLocation();
            String type = sprinklerData.get(position).getType();
            String register = sprinklerData.get(position).getRegister();

            GeneratePopUp.commissioningPopUp(mActivity, equipment, date, location, type, register);
        }
    }

    public ListGenerateDataMainAdapter(Activity mActivity, ArrayList<HoseCabinetModel> sprinklerData) {
        super();
        this.mActivity = mActivity;
        this.sprinklerData = sprinklerData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_generate_data, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        CardView listCV = holder.listCV;
        TextView equipmentTV = holder.equipmentTV;
        TextView locationTV = holder.locationTV;
        TextView dateTV = holder.dateTV;
        TextView registerTV = holder.registerTV;
        TextView typeTV = holder.typeTV;

        equipmentTV.setText(sprinklerData.get(position).getEquipment());
        locationTV.setText("Equipment Location : " + sprinklerData.get(position).getLocation());
        dateTV.setText("Inspection Date : " + sprinklerData.get(position).getDate());
        typeTV.setText("Equipment Type : " + sprinklerData.get(position).getType());
        registerTV.setText("Equipment Register : " + sprinklerData.get(position).getRegister());
    }

    @Override
    public int getItemCount() {
        return sprinklerData.size();
    }
}
