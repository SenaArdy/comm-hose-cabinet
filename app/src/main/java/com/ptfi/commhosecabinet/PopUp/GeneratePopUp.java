package com.ptfi.commhosecabinet.PopUp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.client.android.CaptureActivity;
import com.ptfi.commhosecabinet.Adapter.ListCommTestAdapter;
import com.ptfi.commhosecabinet.Adapter.ListComplianceAdapter;
import com.ptfi.commhosecabinet.Adapter.ListPhotoAdapter;
import com.ptfi.commhosecabinet.Adapter.ListTermsAdapter;
import com.ptfi.commhosecabinet.Database.DataSource;
import com.ptfi.commhosecabinet.Models.CommissioningModel;
import com.ptfi.commhosecabinet.Models.ComplianceModel;
import com.ptfi.commhosecabinet.Models.DataSingleton;
import com.ptfi.commhosecabinet.Models.HandoverModel;
import com.ptfi.commhosecabinet.Models.HoseCabinetModel;
import com.ptfi.commhosecabinet.Models.InspectorModel;
import com.ptfi.commhosecabinet.Models.PhotoModel;
import com.ptfi.commhosecabinet.Models.TermsModel;
import com.ptfi.commhosecabinet.R;
import com.ptfi.commhosecabinet.Utils.Helper;
import com.ptfi.commhosecabinet.Utils.Report;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by senaardyputra on 8/1/16.
 */
public class GeneratePopUp {

    static String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    static int REQUEST_SCAN_BARCODE_CLIENT = 347;
    static int REQUEST_SCAN_BARCODE_ENG = 348;
    static int REQUEST_SCAN_BARCODE_MAIN = 349;
    static int REQUEST_SCAN_BARCODE_AO = 350;
    static int REQUEST_SCAN_BARCODE_MAINRES = 351;
    static int REQUEST_SCAN_BARCODE_CSE = 352;
    static int REQUEST_SCAN_BARCODE_DEPT = 353;

    static int REQUEST_SCAN_BARCODE_CLIENT_HO = 354;
    static int REQUEST_SCAN_BARCODE_ENG_HO = 355;
    static int REQUEST_SCAN_BARCODE_MAIN_HO = 356;
    static int REQUEST_SCAN_BARCODE_AO_HO = 357;
    static int REQUEST_SCAN_BARCODE_MAINRES_HO = 358;
    static int REQUEST_SCAN_BARCODE_CSE_HO = 359;
    static int REQUEST_SCAN_BARCODE_DEPT_HO = 360;

    static PhotoModel activeFModel = null;

    Dialog activeDialog;

    static String inspectorIDFalse;

    static ArrayList<InspectorModel> dataInspector;

    public static EditText equipmentET, dateET, clientET, typeET, registerET, locationET, remark1ET, remark2ET, remark3ET, remark4ET, engineeringET, maintenanceET,
            areaOwnerET, ugmrET, cseET, deptHeadET;

    static TextInputLayout equipmentTL, dateTL, clientTL, typeTL, registerTL, locationTL, remark1TL, remark2TL, remark3TL, remark4TL, engineeringTL, maintenanceTL,
            areaOwnerTL, ugmrTL, cseTL, deptHeadTL;

    static MultiStateToggleButton insp1, insp2, insp3, insp4;

    static TextView Q80TV, Q90TV, Q100TV, Q110TV;

    static ImageView addComplianceIV, addPhotosIV;

    static LinearLayout findingsLL, photosLL;

    static RecyclerView findingsRV, photosRV, commTestRV;

    static TextView imeiCode, versionCode;

    static CardView generateCV, previewCV;

    static PopUp.page pages;
    static PopUp.signature signatures;

    static RecyclerView.LayoutManager layoutManager;
    static RecyclerView.LayoutManager photosLayoutManager;

    private static ArrayList<CommissioningModel> commissioningData = new ArrayList<>();
    static ArrayList<ComplianceModel> complianceData = new ArrayList<>();
    static ArrayList<PhotoModel> photoData = new ArrayList<>();

    static ListCommTestAdapter commData;
    static ListComplianceAdapter adapterData;
    static ListPhotoAdapter adapterPhotosData;

    static int selectedYear = 0;
    static int selectedMonth = 0;
    static int selectedDate = 0;

    static EditText addTermsET;

    static TextInputLayout addTermsTL;

    static LinearLayout termsLL;

    static RecyclerView termsRV;

    static CardView addCV;

    static ListTermsAdapter adapterTermData;

    private static ArrayList<TermsModel> termsData = new ArrayList<>();

    public static void commissioningPopUp(final Activity mActivity, final String equipment, final String date, final String location,
                                          final String type, final String register) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_main);

                versionCode = (TextView) dialog.findViewById(R.id.versionCode);
                PackageInfo pInfo;
                try {
                    pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
                    String version = pInfo.versionName;
                    versionCode.setText("Version : " + version);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                // Imei
                TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
                String deviceID = telephonyManager.getDeviceId();
                imeiCode = (TextView) dialog.findViewById(R.id.imeiCode);
                imeiCode.setText("Device IMEI : " + deviceID);

                equipmentET = (EditText) dialog.findViewById(R.id.equipmentET);
                registerET = (EditText) dialog.findViewById(R.id.registerET);
                dateET = (EditText) dialog.findViewById(R.id.dateET);
                clientET = (EditText) dialog.findViewById(R.id.clientET);
                typeET = (EditText) dialog.findViewById(R.id.typeEqET);
                locationET = (EditText) dialog.findViewById(R.id.locationEqET);

                equipmentTL = (TextInputLayout) dialog.findViewById(R.id.equipmentTL);
                registerTL = (TextInputLayout) dialog.findViewById(R.id.registerTL);
                dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);
                clientTL = (TextInputLayout) dialog.findViewById(R.id.clientTL);
                typeTL = (TextInputLayout) dialog.findViewById(R.id.typeEqTL);
                locationTL = (TextInputLayout) dialog.findViewById(R.id.locationEqTL);

                engineeringET = (EditText) dialog.findViewById(R.id.engineeringET);
                maintenanceET = (EditText) dialog.findViewById(R.id.maintenanceET);
                areaOwnerET = (EditText) dialog.findViewById(R.id.areaOwnerET);
                ugmrET = (EditText) dialog.findViewById(R.id.ugmrET);
                cseET = (EditText) dialog.findViewById(R.id.cseET);
                deptHeadET = (EditText) dialog.findViewById(R.id.deptHeadET);

                engineeringTL = (TextInputLayout) dialog.findViewById(R.id.engineeringTL);
                maintenanceTL = (TextInputLayout) dialog.findViewById(R.id.maintenanceTL);
                areaOwnerTL = (TextInputLayout) dialog.findViewById(R.id.areaOwnerTL);
                ugmrTL = (TextInputLayout) dialog.findViewById(R.id.ugmrTL);
                cseTL = (TextInputLayout) dialog.findViewById(R.id.cseTL);
                deptHeadTL = (TextInputLayout) dialog.findViewById(R.id.deptHeadTL);

                insp1 = (MultiStateToggleButton) dialog.findViewById(R.id.insp1);
                insp2 = (MultiStateToggleButton) dialog.findViewById(R.id.insp2);
                insp3 = (MultiStateToggleButton) dialog.findViewById(R.id.insp3);
                insp4 = (MultiStateToggleButton) dialog.findViewById(R.id.insp4);

                remark1TL = (TextInputLayout) dialog.findViewById(R.id.remark1TL);
                remark2TL = (TextInputLayout) dialog.findViewById(R.id.remark2TL);
                remark3TL = (TextInputLayout) dialog.findViewById(R.id.remark3TL);
                remark4TL = (TextInputLayout) dialog.findViewById(R.id.remark4TL);

                remark1ET = (EditText) dialog.findViewById(R.id.remark1ET);
                remark2ET = (EditText) dialog.findViewById(R.id.remark2ET);
                remark3ET = (EditText) dialog.findViewById(R.id.remark3ET);
                remark4ET = (EditText) dialog.findViewById(R.id.remark4ET);

                Q80TV = (TextView) dialog.findViewById(R.id.Q80TV);
                Q90TV = (TextView) dialog.findViewById(R.id.Q90TV);
                Q100TV = (TextView) dialog.findViewById(R.id.Q100TV);
                Q110TV = (TextView) dialog.findViewById(R.id.Q110TV);

                final DataSource dataSource = new DataSource(mActivity);
                dataSource.open();

                HoseCabinetModel dataComm = dataSource.getDataCommissioning(equipment, date, location, type, register);

                equipmentET.setText(equipment);
                dateET.setText(date);
                locationET.setText(location);
                typeET.setText(type);
                registerET.setText(register);
                clientET.setText(dataComm.getClient());

                if(dataComm.getContractor() == null) {
                    cseTL.setHint("Installer");
                } else {
                    cseTL.setHint(dataComm.getContractor());
                }

                if(dataComm.getDivision() == null) {
                    ugmrTL.setHint("Team Response");
                } else {
                    ugmrTL.setHint(dataComm.getDivision());
                }

                engineeringET.setText(dataComm.getNameEng());
                maintenanceET.setText(dataComm.getNameMain());
                areaOwnerET.setText(dataComm.getNameAO());
                ugmrET.setText(dataComm.getNameUGMR());
                cseET.setText(dataComm.getNameCSE());
                deptHeadET.setText(dataComm.getNameDept());

                if(dataComm.getQuestion1().toString().equalsIgnoreCase("Pass")) {
                    insp1.setValue(0);
                } else if (dataComm.getQuestion1().toString().equalsIgnoreCase("No")) {
                    insp1.setValue(1);
                } else {
                    insp1.setValue(2);
                }
                remark1ET.setText(dataComm.getRemark1());

                if(dataComm.getQuestion2().toString().equalsIgnoreCase("Pass")) {
                    insp2.setValue(0);
                } else if (dataComm.getQuestion2().toString().equalsIgnoreCase("No")) {
                    insp2.setValue(1);
                } else {
                    insp2.setValue(2);
                }
                remark2ET.setText(dataComm.getRemark2());

                if(dataComm.getQuestion3().toString().equalsIgnoreCase("Pass")) {
                    insp3.setValue(0);
                } else if (dataComm.getQuestion3().toString().equalsIgnoreCase("No")) {
                    insp3.setValue(1);
                } else {
                    insp3.setValue(2);
                }
                remark3ET.setText(dataComm.getRemark3());

                if(dataComm.getQuestion4().toString().equalsIgnoreCase("Pass")) {
                    insp4.setValue(0);
                } else if (dataComm.getQuestion4().toString().equalsIgnoreCase("No")) {
                    insp4.setValue(1);
                } else {
                    insp4.setValue(2);
                }
                remark4ET.setText(dataComm.getRemark4());

                activeFModel = new PhotoModel();

                addComplianceIV = (ImageView) dialog.findViewById(R.id.addComplianceIV);
                addComplianceIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopUp.showCompliance(mActivity, equipmentET.getText().toString(), dateET.getText().toString(), registerET.getText().toString(),
                                null, null, null, null, true, new PopUpComplianceCallback() {
                                    @Override
                                    public void onDataSave(ComplianceModel model) {
                                        DataSource ds = new DataSource(mActivity);
                                        ds.open();

                                        ds.insertCompliance(model);

                                        layoutManager = new LinearLayoutManager(mActivity);
                                        findingsRV.setLayoutManager(layoutManager);
                                        findingsRV.setItemAnimator(new DefaultItemAnimator());

                                        complianceData = ds.getAllComplianceEquipment(equipmentET.getText().toString(),
                                                dateET.getText().toString(), registerET.getText().toString());

                                        adapterData = new ListComplianceAdapter(mActivity, complianceData);
                                        findingsRV.setAdapter(adapterData);

                                        ds.close();
                                    }
                                });
                    }
                });

                addPhotosIV = (ImageView) dialog.findViewById(R.id.addPhotosIV);

                addPhotosIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhotoPopUp.showAddImagePopUp(mActivity, equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString(), new PopUpPhotosCallback() {
                                    @Override
                                    public void onDataSave(PhotoModel model) {
                                        DataSource ds = new DataSource(mActivity);
                                        ds.open();

                                        ds.insertPhotos(model);

                                        photosLayoutManager = new LinearLayoutManager(mActivity);
                                        photosRV.setLayoutManager(photosLayoutManager);
                                        photosRV.setItemAnimator(new DefaultItemAnimator());

                                        photoData = ds.getAllPhotoData(equipmentET.getText().toString(),
                                                dateET.getText().toString(), registerET.getText().toString());

                                        adapterPhotosData = new ListPhotoAdapter(mActivity, photoData);
                                        photosRV.setAdapter(adapterPhotosData);

                                        ds.close();
                                    }
                                });
                    }
                });

                addPhotosIV = (ImageView) dialog.findViewById(R.id.addPhotosIV);

                findingsLL = (LinearLayout) dialog.findViewById(R.id.findingsLL);
                photosLL = (LinearLayout) dialog.findViewById(R.id.photosLL);

                commTestRV = (RecyclerView) dialog.findViewById(R.id.commTestRV);
                findingsRV = (RecyclerView) dialog.findViewById(R.id.findingsRV);
                photosRV = (RecyclerView) dialog.findViewById(R.id.photosRV);

                commissioningData = dataSource.getAllCommEquipment(equipmentET.getText().toString(),
                        dateET.getText().toString(), registerET.getText().toString());

                if(commissioningData.size() > 0) {
                    commTestRV.setLayoutManager(new LinearLayoutManager(mActivity));
                    findingsRV.setItemAnimator(new DefaultItemAnimator());

                    commData = new ListCommTestAdapter(mActivity, commissioningData);
                    commTestRV.setAdapter(commData);
                }

                complianceData = dataSource.getAllComplianceEquipment(equipmentET.getText().toString(),
                        dateET.getText().toString(), registerET.getText().toString());

                if (complianceData.size() > 0) {
//                    layoutManager = new LinearLayoutManager(mActivity);
                    findingsRV.setLayoutManager(new LinearLayoutManager(mActivity));
                    findingsRV.setItemAnimator(new DefaultItemAnimator());

                    adapterData = new ListComplianceAdapter(mActivity, complianceData);
                    findingsRV.setAdapter(adapterData);
                }

                photoData = dataSource.getAllPhotoData(equipmentET.getText().toString(),
                        dateET.getText().toString(), registerET.getText().toString());

                if (photoData.size() > 0) {
//                    photosLayoutManager = new LinearLayoutManager(mActivity);
                    photosRV.setLayoutManager(new LinearLayoutManager(mActivity));
                    photosRV.setItemAnimator(new DefaultItemAnimator());

                    adapterPhotosData = new ListPhotoAdapter(mActivity, photoData);
                    photosRV.setAdapter(adapterPhotosData);
                }

                dataSource.close();

                generateCV = (CardView) dialog.findViewById(R.id.generateCV);
                previewCV = (CardView) dialog.findViewById(R.id.previewCV);

                generateCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(validation()) {
                            Report.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), true);
                        }
                    }
                });

                previewCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(validation()) {
                            Report.createCommissioningReport(mActivity, equipmentET.getText().toString(),
                                    dateET.getText().toString(), locationET.getText().toString(), typeET.getText().toString(),
                                    registerET.getText().toString(), false);
                        }
                    }
                });

                Q80TV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(findingsRV.getVisibility() == View.GONE) {
                            findingsRV.setVisibility(View.VISIBLE);
                        } else if ( findingsRV.getVisibility() == View.VISIBLE) {
                            findingsRV.setVisibility(View.GONE);
                        }
                    }
                });

                Q90TV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(photosRV.getVisibility() == View.GONE) {
                            photosRV.setVisibility(View.VISIBLE);
                        } else if ( photosRV.getVisibility() == View.VISIBLE) {
                            photosRV.setVisibility(View.GONE);
                        }
                    }
                });

//                clientET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveData(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBar(signatures, mActivity);
//                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateData(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBar(signatures, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });
//
//                engineeringET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveData(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                scanBar(signatures, mActivity);
//                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateData(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                scanBar(signatures, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });
//
                maintenanceET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                saveData(mActivity);
                                pages = PopUp.page.commissioning;
                                signatures = PopUp.signature.maintenance;
                                scanBar(signatures, pages, mActivity);
                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                updateData(mActivity);
                                signatures = PopUp.signature.maintenance;
                                scanBar(signatures, pages, mActivity);
                            }
                            ds.close();
                        }
                    }
                });

                areaOwnerET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            final DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HoseCabinetModel model, String name, String division) {
                                            ds.updateAOCommissioning(model);
                                            areaOwnerET.setText(name);
                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                            dataSource.close();
                        }
                    }
                });

                ugmrET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            final  DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HoseCabinetModel model, String name, String division) {
                                            dataSource.updateMainResCommissioning(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                            dataSource.close();
                        }
                    }
                });

                cseET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            final  DataSource ds = new DataSource(mActivity);
                            ds.open();
                            PopUp.page pages = PopUp.page.commissioning;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(HoseCabinetModel model, String name, String division) {
                                            dataSource.updateCSECommissioning(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {

                                        }
                                    });
                            dataSource.close();
                        }
                    }
                });

                deptHeadET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (validation()) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            if (!ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                saveData(mActivity);
                                signatures = PopUp.signature.deptHead;
                                pages = PopUp.page.commissioning;
                                scanBar(signatures, pages, mActivity);
                            } else if (ds.validateDataCommissioning(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString())) {
                                updateData(mActivity);
                                signatures = PopUp.signature.deptHead;
                                scanBar(signatures, pages, mActivity);
                            }
                            ds.close();
                        }
                    }
                });

                final Calendar c = Calendar.getInstance();
                selectedYear = c.get(Calendar.YEAR);
                selectedMonth = c.get(Calendar.MONTH);
                selectedDate = c.get(Calendar.DAY_OF_MONTH);
                DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                        selectedDate);
                dateET.setText(DataSingleton.getInstance().getFormattedDate());

                dialog.show();
            }
        });
    }

    public static void datePickers(final Activity mActivity, final View dialog) {
        Helper.showDatePicker(dialog, (FragmentActivity) mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        selectedDate = dayOfMonth;

                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                        ((EditText) dialog).setText(DataSingleton.getInstance()
                                .getFormattedDate());
                    }
                }, selectedYear, selectedMonth, selectedDate);
    }

    public static boolean validation() {
        boolean status = true;

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Sprinkler Equipment");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Sprinkler Location");
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Sprinkler Type");
        }

//        if (registerET.getText().toString().length() == 0) {
//            status = false;
//            registerTL.setErrorEnabled(true);
//            registerTL.setError("*Please fill Sprinkler Register");
//        }

        return status;
    }

    public static void saveData(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        HoseCabinetModel model = new HoseCabinetModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }

        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());

        ds.insertHydrant(model);
        ds.close();
    }

    public static void updateData(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        HoseCabinetModel model = new HoseCabinetModel();

        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        if (insp1.getValue() == 0) {
            model.setQuestion1("Pass");
        } else if (insp1.getValue() == 1) {
            model.setQuestion1("No");
        } else {
            model.setQuestion1("N/A");
        }
        if (insp2.getValue() == 0) {
            model.setQuestion2("Pass");
        } else if (insp2.getValue() == 1) {
            model.setQuestion2("No");
        } else {
            model.setQuestion2("N/A");
        }
        if (insp3.getValue() == 0) {
            model.setQuestion3("Pass");
        } else if (insp3.getValue() == 1) {
            model.setQuestion3("No");
        } else {
            model.setQuestion3("N/A");
        }
        if (insp4.getValue() == 0) {
            model.setQuestion4("Pass");
        } else if (insp4.getValue() == 1) {
            model.setQuestion4("No");
        } else {
            model.setQuestion4("N/A");
        }

        model.setRemark1(remark1ET.getText().toString());
        model.setRemark2(remark2ET.getText().toString());
        model.setRemark3(remark3ET.getText().toString());
        model.setRemark4(remark4ET.getText().toString());

        ds.updateHydrantData(model);
        ds.close();
    }

    private static android.app.AlertDialog showQRDialog(final Activity act,
                                                        CharSequence title, CharSequence message, CharSequence buttonYes,
                                                        CharSequence buttonNo) {
        android.app.AlertDialog.Builder downloadDialog = new android.app.AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helper.copyAndOpenBarcodeScannerAPK(act);
                    }
                });
        downloadDialog.setNegativeButton(buttonNo,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        return downloadDialog.show();
    }

    public static void scanBar(PopUp.signature signatures, PopUp.page pages, final Activity mActivity) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
//                case engineering:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG);
//                    break;

                case maintenance:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN);
                    break;

//                case areaOwner:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_AO);
//                    break;
//
//                case mainRes:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAINRES);
//                    break;
//
//                case CSE:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CSE);
//                    break;

                case deptHead:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_DEPT);
                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }

    public static void handOverPopUp (final Activity mActivity, final String equipment, final String date,
                                      final String location, final String type, final String register) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_handover);

                equipmentET = (EditText) dialog.findViewById(R.id.equipmentET);
                registerET = (EditText) dialog.findViewById(R.id.registerET);
                dateET = (EditText) dialog.findViewById(R.id.dateET);
                clientET = (EditText) dialog.findViewById(R.id.clientET);
                typeET = (EditText) dialog.findViewById(R.id.typeEqET);
                locationET = (EditText) dialog.findViewById(R.id.locationEqET);
                addTermsET = (EditText) dialog.findViewById(R.id.addTermsET);

                equipmentTL = (TextInputLayout) dialog.findViewById(R.id.equipmentTL);
                registerTL = (TextInputLayout) dialog.findViewById(R.id.registerTL);
                dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);
                clientTL = (TextInputLayout) dialog.findViewById(R.id.clientTL);
                typeTL = (TextInputLayout) dialog.findViewById(R.id.typeEqTL);
                locationTL = (TextInputLayout) dialog.findViewById(R.id.locationEqTL);
                addTermsTL = (TextInputLayout) dialog.findViewById(R.id.addTermsTL);

                engineeringET = (EditText) dialog.findViewById(R.id.engineeringET);
                maintenanceET = (EditText) dialog.findViewById(R.id.maintenanceET);
                areaOwnerET = (EditText) dialog.findViewById(R.id.areaOwnerET);
                ugmrET = (EditText) dialog.findViewById(R.id.ugmrET);
                cseET = (EditText) dialog.findViewById(R.id.cseET);
                deptHeadET = (EditText) dialog.findViewById(R.id.deptHeadET);

                engineeringTL = (TextInputLayout) dialog.findViewById(R.id.engineeringTL);
                maintenanceTL = (TextInputLayout) dialog.findViewById(R.id.maintenanceTL);
                areaOwnerTL = (TextInputLayout) dialog.findViewById(R.id.areaOwnerTL);
                ugmrTL = (TextInputLayout) dialog.findViewById(R.id.ugmrTL);
                cseTL = (TextInputLayout) dialog.findViewById(R.id.cseTL);
                deptHeadTL = (TextInputLayout) dialog.findViewById(R.id.deptHeadTL);

                termsLL = (LinearLayout) dialog.findViewById(R.id.termsLL);

                termsRV = (RecyclerView) dialog.findViewById(R.id.termsRV);

                DataSource dataSource = new DataSource(mActivity);
                dataSource.open();

                HandoverModel hoData = new HandoverModel();
                hoData = dataSource.getDataHandOver(equipment,date, location, type, register);

                equipmentET.setText(equipment);
                dateET.setText(date);
                locationET.setText(location);
                typeET.setText(type);
                registerET.setText(register);
                clientET.setText(hoData.getClient());

                if(hoData.getContractor() == null) {
                    cseTL.setHint("Installer");
                } else {
                    cseTL.setHint(hoData.getContractor());
                }

                if(hoData.getDivision() == null) {
                    ugmrTL.setHint("Team Response");
                } else {
                    ugmrTL.setHint(hoData.getDivision());
                }

                termsData = dataSource.getAllTermsEquipment(equipmentET.getText().toString(),
                        dateET.getText().toString(), registerET.getText().toString());

                if (termsData.size() > 0) {
                    termsRV.setHasFixedSize(true);
                    layoutManager = new LinearLayoutManager(mActivity);
                    termsRV.setLayoutManager(layoutManager);
                    termsRV.setItemAnimator(new DefaultItemAnimator());

                    adapterTermData = new ListTermsAdapter(mActivity, termsData);
                    termsRV.setAdapter(adapterTermData);
                }

                engineeringET.setText(hoData.getNameEng());
                maintenanceET.setText(hoData.getNameMain());
                areaOwnerET.setText(hoData.getNameAO());
                ugmrET.setText(hoData.getNameUGMR());
                cseET.setText(hoData.getNameCSE());
                deptHeadET.setText(hoData.getNameDept());

                addCV = (CardView) dialog.findViewById(R.id.addCV);
                addCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (addTermsET.getText().toString().length() >= 1) {
                            DataSource ds = new DataSource(mActivity);
                            ds.open();
                            TermsModel model = new TermsModel();

                            model.setTerms(addTermsET.getText().toString());
                            model.setEquipment(equipmentET.getText().toString());
                            model.setDate(dateET.getText().toString());
                            model.setRegister(registerET.getText().toString());

                            ds.insertTerms(model);

                            termsRV.setHasFixedSize(true);
                            layoutManager = new LinearLayoutManager(mActivity);
                            termsRV.setLayoutManager(layoutManager);
                            termsRV.setItemAnimator(new DefaultItemAnimator());

                            termsData = ds.getAllTermsEquipment(equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString());

                            adapterTermData = new ListTermsAdapter(mActivity, termsData);
                            termsRV.setAdapter(adapterData);

                            ds.close();

                            addTermsET.setText("");
                        } else if (addTermsET.getText().toString().length() == 0) {
                            addTermsTL.setErrorEnabled(true);
                            addTermsTL.setError("*Please fill this Terms and Conditions");
                        }
                    }
                });
                generateCV = (CardView) dialog.findViewById(R.id.generateCV);
                previewCV = (CardView) dialog.findViewById(R.id.previewCV);

                generateCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Report.createHandOverReport(mActivity, equipment, date, location, type, register, true);
                    }
                });

                previewCV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Report.createHandOverReport(mActivity, equipment, date, location, type, register, false);
                    }
                });

//                clientET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveDataHandOver(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBarHO(signatures,mActivity);
//                            } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateDataHandOver(mActivity);
//                                signatures = PopUp.signature.client;
//                                scanBarHO(signatures, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });
//
//                engineeringET.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (validation()) {
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//                            if (!ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                saveDataHandOver(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                pages = PopUp.page.handover;
//                                scanBarHO(signatures, pages, mActivity);
//                            } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
//                                    dateET.getText().toString(), registerET.getText().toString())) {
//                                updateDataHandOver(mActivity);
//                                signatures = PopUp.signature.engineering;
//                                pages = PopUp.page.handover;
//                                scanBarHO(signatures, pages, mActivity);
//                            }
//                            ds.close();
//                        }
//                    }
//                });

                maintenanceET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveDataHandOver(mActivity);
                            signatures = PopUp.signature.maintenance;
                            pages = PopUp.page.handover;
                            scanBarHO(signatures, pages, mActivity);
                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateDataHandOver(mActivity);
                            signatures = PopUp.signature.maintenance;
                            pages = PopUp.page.handover;
                            scanBarHO(signatures, pages, mActivity);
                        }
                        ds.close();
                    }
                });

                areaOwnerET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final  DataSource ds = new DataSource(mActivity);
                        ds.open();
                        PopUp.page pages = PopUp.page.handover;
                        PopUp.signature signatures = PopUp.signature.areaOwner;
                        PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                    @Override
                                    public void onDataSave(HoseCabinetModel model, String name, String division) {

                                    }

                                    @Override
                                    public void onDataSaveHO(HandoverModel model, String name, String division) {
                                        ds.updateAOHandOver(model);
                                        areaOwnerET.setText(name);

                                    }
                                });
                        ds.close();
                    }
                });
//
                ugmrET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        PopUp.page pages = PopUp.page.handover;
                        PopUp.signature signatures = PopUp.signature.mainRes;
                        PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                    @Override
                                    public void onDataSave(HoseCabinetModel model, String name, String division) {

                                    }

                                    @Override
                                    public void onDataSaveHO(HandoverModel model, String name, String division) {
                                        ds.updateMainResHandOver(model);
                                        ugmrTL.setHint(division);
                                        ugmrET.setText(name);
                                    }
                                });
                        ds.close();
                    }
                });

                cseET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final  DataSource ds = new DataSource(mActivity);
                        ds.open();
                        PopUp.page pages = PopUp.page.handover;
                        PopUp.signature signatures = PopUp.signature.CSE;
                        PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                    @Override
                                    public void onDataSave(HoseCabinetModel model, String name, String division) {

                                    }

                                    @Override
                                    public void onDataSaveHO(HandoverModel model, String name, String division) {
                                        ds.updateCSEHandOver(model);
                                        cseTL.setHint(division);
                                        cseET.setText(name);
                                    }
                                });
                        ds.close();
                    }
                });

                deptHeadET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveDataHandOver(mActivity);
                            signatures = PopUp.signature.deptHead;
                            pages = PopUp.page.handover;
                            scanBarHO(signatures, pages, mActivity);
                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateDataHandOver(mActivity);
                            signatures = PopUp.signature.deptHead;
                            pages = PopUp.page.handover;
                            scanBarHO(signatures, pages, mActivity);
                        }
                        ds.close();
                    }
                });

                // Date
                final Calendar c = Calendar.getInstance();
                selectedYear = c.get(Calendar.YEAR);
                selectedMonth = c.get(Calendar.MONTH);
                selectedDate = c.get(Calendar.DAY_OF_MONTH);
                DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                        selectedDate);
                dateET.setText(DataSingleton.getInstance().getFormattedDate());

                dialog.show();
            }
        });
    }

    public static void saveDataHandOver(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        HandoverModel model = new HandoverModel();
        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        ds.insertHandOver(model);

        ds.close();
    }

    public static void updateDataHandOver(final Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        HandoverModel model = new HandoverModel();
        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        ds.updateHandOverData(model);

        ds.close();
    }

    public static void scanBarHO(PopUp.signature signatures, PopUp.page pages, final Activity mActivity) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
//                case engineering:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG_HO);
//                    break;

                case maintenance:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN_HO);
                    break;

//                case areaOwner:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_AO_HO);
//                    break;
//
//                case mainRes:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAINRES_HO);
//                    break;
//
//                case CSE:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CSE_HO);
//                    break;

                case deptHead:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_DEPT_HO);
                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }
}
