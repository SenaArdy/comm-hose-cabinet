package com.ptfi.commhosecabinet.PopUp;

import com.ptfi.commhosecabinet.Models.CommissioningModel;

/**
 * Created by senaardyputra on 7/30/16.
 */
public interface PopUpCommTestCallBack {
    public void onDataSave(CommissioningModel model);
}
