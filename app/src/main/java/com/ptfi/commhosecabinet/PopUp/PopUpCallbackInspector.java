package com.ptfi.commhosecabinet.PopUp;

import com.ptfi.commhosecabinet.Models.HandoverModel;
import com.ptfi.commhosecabinet.Models.HoseCabinetModel;

/**
 * Created by senaardyputra on 7/27/16.
 */
public interface PopUpCallbackInspector {
    public void onDataSave(HoseCabinetModel model, String name, String division);

    public void onDataSaveHO(HandoverModel model, String name, String division);
}
