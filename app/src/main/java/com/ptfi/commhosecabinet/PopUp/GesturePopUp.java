package com.ptfi.commhosecabinet.PopUp;

import android.app.Activity;
import android.app.Dialog;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.ptfi.commhosecabinet.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by senaardyputra on 8/1/16.
 */
public class GesturePopUp {

    public static void showGesture(final Activity mActivity, final GestureCallback callback) {
        mActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.gesture_layout);
                dialog.setCanceledOnTouchOutside(false);

                final GestureOverlayView gesture = (GestureOverlayView) dialog.findViewById(R.id.gestures);
                Button button_save = (Button) dialog.findViewById(R.id.save_button);
                Button button_clear = (Button) dialog.findViewById(R.id.clear_button);
                Button button_close = (Button) dialog.findViewById(R.id.close_button);

                button_close.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                button_clear.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        gesture.cancelClearAnimation();
                        gesture.clear(true);
                    }
                });

                button_save.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        Bitmap bitmap = null;
                        try {
                            Bitmap gestureImg = gesture.getGesture().toBitmap(60, 45,
                                    8, Color.BLACK);

                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            gestureImg.compress(Bitmap.CompressFormat.PNG, 100, bos);
                            byte[] bArray = bos.toByteArray();

                            ByteArrayInputStream imageStreamClient = new ByteArrayInputStream(bArray);
                            bitmap = BitmapFactory.decodeStream(imageStreamClient);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (callback != null) {
                            callback.onBitmapSaved(bitmap);
                        }

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }
}
