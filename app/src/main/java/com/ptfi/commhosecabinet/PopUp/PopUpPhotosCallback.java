package com.ptfi.commhosecabinet.PopUp;

import com.ptfi.commhosecabinet.Models.PhotoModel;

/**
 * Created by senaardyputra on 7/18/16.
 */
public interface PopUpPhotosCallback {
    public void onDataSave(PhotoModel model);
}
