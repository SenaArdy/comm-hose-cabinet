package com.ptfi.commhosecabinet.PopUp;

import com.ptfi.commhosecabinet.Models.ComplianceModel;

/**
 * Created by senaardyputra on 6/18/16.
 */
public interface PopUpComplianceCallback {
    public void onDataSave(ComplianceModel model);
}
